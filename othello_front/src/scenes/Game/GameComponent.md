Le component Game est celui qui contient la logique du jeu

À l'affichage de la page un tableau de jeu contenant 8 rangées et 8 colonnes

[
    [][][][][][][][]
    [][][][][][][][]
    [][][][][][][][]
    [][][][][][][][]
    [][][][][][][][]
    [][][][][][][][]
    [][][][][][][][]
    [][][][][][][][]
]

Les quatre cases du milieu contiennent 1 pion:
 -[row = 3] et [column = 3] -> pion Noir
 -[row = 4] et [column = 3] -> pion Blanc
 -[row = 3] et [column = 4] -> pion Blanc
 -[row = 4] et [column = 4] -> pion Noir

Au début du jeu voici le tableau qu'il faut avoir:

[
    [][][][][][][][]
    [][][][][][][][]
    [][][][][][][][]
   [][][][b][w][][][]
   [][][][w][b][][][]
    [][][][][][][][]
    [][][][][][][][]
    [][][][][][][][]
]

La fonction squareHasContent prend en paramètre (l'index de la rangée, l'index de la colonne), elle retourne un nombre
qui correspond à la longeur du contenu d'une case.

Si la fonction renvoit 0 la case est vide si elle renvoit 1 si la case contient un pion.

/*FONCTION*/

    squareHasContent = (row, column) => {
        return this.state.ground[row][column].length
    }


La fonction putPawnsInSquare prend en paramètre (l'index de la rangée, l'index de la colonne), elle permet au joueur d'ajouter un pion, elle utilise la fonction squareHasContent pour vérifier si la case contient un pion, si la case contient un pion l'emplacement devient indisponible sinon le joueur peut ajouter un pion.

/*FONCTION*/

    putPawnsInSquare = (row, column) => {
        //If square is empty I can add a pawn in it
        if (this.squareHasContent(row, column) === 0){
            this.state.ground[row][column].push('B')
            this.setState({ ground: this.state.ground })
        } else {
            console.log('on ne peut pas ajouter de pion')
        }
    }