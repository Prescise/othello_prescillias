import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import Black from './components/black-pawn.png'
import White from './components/white-pawn.png'
import { Button } from 'semantic-ui-react'

import socketIOClient from 'socket.io-client'

import './Game.scss'

class Game extends Component {
    constructor(props) {
        super(props)
        this.state = {
            message: '',
            nb_player: 0,
            ground: [],
            tutu: [],
            endpoint: "http://127.0.0.1:4000",
            player1: {
                name: 'player1',
                pawns_number: 32,
                color: Black,
                pass_turn: {
                    nb: 0,
                    round: 0
                }
            },
            player2: {
                name: 'player2',
                pawns_number: 32,
                color: White,
                pass_turn: {
                    nb: 0,
                    round: 0
                }
            },
            round: 1,
            currentPlayer: 'Player1'
        }
    }

    componentDidMount = () => {
        //When a player is connected get a message
        const socket = socketIOClient(this.state.endpoint)
        let playerConnected = this.state.nb_player

        socket.on('user_connected', () => {
            if (playerConnected = 0){  //Count how many Players connected to start a game

                this.setState({ message: 'Le joueur 1 est connecté' })
                playerConnected += 1 
                this.setState({ nb_player: playerConnected})

                console.log(this.state.nb_player)


            } else if(this.state.nb_player = 1)

                this.setState({ message: 'Le joueur 2 est connecté' })
                playerConnected += 1 
                this.setState({ nb_player: playerConnected})

                console.log(this.state.nb_player)

            }
        )

        socket.on('user_disconnect', () => {
            if (this.state.nb_player = 1){  //Count how many Players connected to start a game

                this.setState({ message_end_game: 'Un joueur a quitté la partie' })
                playerConnected -= 1 
                this.setState({ nb_player: playerConnected})

                console.log(this.state.nb_player)


            }
        })
        
        //Create a 8 x 8 ground with 64 squares
        let modelGround = []
        for (let row = 0, lr = 8; row < lr; row++){
            modelGround.push([]) 
            for (let column = 0, lc = 8; column < lc; column++){ // We create 8 arrays for the columns
                modelGround[row].push([])
            }
        }

        //At the begining of the game four Squares in the middle are full
        modelGround[3][3].push(Black)
        modelGround[3][4].push(White)
        modelGround[4][3].push(White)
        modelGround[4][4].push(Black)

        this.setState({ ground: modelGround })
        this.checkWhoPlay()
    }

    checkWhoPlay = () => {
        return this.state.round % 2 === 0 ? this.state.player2.name : this.state.player1.name
    }

    getColor = () => {
        return this.state.round % 2 === 0 ? this.state.player2.color : this.state.player1.color
    }

    squareHasContent = (row, column) => {
        return this.state.ground[row][column].length
    }

    passTurn = () => {
       let user = this.checkWhoPlay()

       if(this.state[user].pass_turn.nb === 1){
           if(this.state[user].pass_turn.round === this.state.round - 2){
               console.log('partie terminée')
           }
           else {
               this.state.pass_turn.round = 0
               this.setState({ player:  this.state[user]})
           }
       }

        this.state[user].pass_turn.nb += 1
        this.state[user].pass_turn.round = this.state.round
        this.state.round += 1
        this.state.currentPlayer = this.checkWhoPlay()
        this.setState({ current: this.state.currentPlayer })
        this.setState({ player:  this.state[user]})
        this.setState({ round: this.state.round })
    }

    putPawnsInSquare = (row, column) => {

        if (this.squareHasContent(row, column) === 0){
            let player = this.checkWhoPlay()
            let updatePawn = this.state[player].pawns_number
            let currentPlayer = this.checkWhoPlay()
            let round = this.state.round

            this.isOpponentPawnAround(row, column)

            updatePawn -= 1

            round += 1

            this.setState({ pawns_number: updatePawn })

            this.setState({ round: round})

            this.setState({ currentPlayer: currentPlayer })
        }
        
    }

    isOpponentPawnAround = (row, column) => {
        let myPawn = this.getColor()
        if(this.squareHasContent(row, column - 1) 
            && this.state.ground[row][column - 1][0] !== myPawn){
                let i = column

                while(i > 0){
                    if(this.state.ground[row][i][0] === myPawn){
                        this.replacePawns(row, column, i)
                    }
                    i--
                }
            }

    }

    replacePawns = (row, column, i) => {
        let ground = this.getColor()

        let y = column
        while( y > i){
            ground = this.getColor()
            this.setState({ ground: ground })
            y--
        }
    }

    render() {
        return (
            <section className="ground-container">
                <div className="ground">
                    {
                        this.state.ground.map(row => //Iteration to display rows
                            <div className="row" key={`${ this.state.ground.indexOf(row)}`}>
                                {
                                    row.map(square => //Iteration to display squares

                                        
                                        square.length ? 
                                            <div className="square" onClick={ () => this.putPawnsInSquare(this.state.ground.indexOf(row), row.indexOf(square)) }
                                                style={{ backgroundColor: 'white' }}
                                                key={ Math.floor(Math.random() * Math.floor(9999999)) }>
                                                    <img alt="paw" src={ this.state.ground[this.state.ground.indexOf(row)][row.indexOf(square)][0]
                                                    === Black ? Black : White
                                                    } />
                                            </div>
                                        :
                                            <div className="square" onClick={ () => this.putPawnsInSquare(this.state.ground.indexOf(row), row.indexOf(square)) }
                                                key={ Math.floor(Math.random() * Math.floor(9999999)) }>
                                            </div>
                                        
                                    ) 
                                }   
                            </div>  
                        )
                    }
                </div>
                <div className="ground-dashboard">
                    <Link to="/">
                        Quitter la partie et retourner à l'accueil
                    </Link>
                    <p>{ `Nombre de tours joués: ${this.state.round}` }</p>
                    <Button className="ui secondary basic button" onClick={ () => this.passTurn() }>Passer ce tour({this.checkWhoPlay()})</Button>

                    <p><strong>{`Player qui est en train de jouer: ${ this.state.currentPlayer }`}</strong></p>
                    <p><strong>{`Player en attente: ${ this.state.round % 2 === 0 ? 'Player 1' : 'Player 2'}`}</strong></p>
                    <p><strong>{`Nombre de pions restants pour le Player 1: ${ this.state.player1.pawns_number }`}</strong></p>
                    <p><strong>{`Nombre de pions restants pour le Player 2: ${ this.state.player2.pawns_number }`}</strong></p>

                    { this.state.nb_player === 1 ? 
                        <p>Joueur 1 est connecté</p>
                        :
                        ''
                    }
                    { this.state.nb_player === 2 ? 
                        <div>
                            <p>Joueur 1 est connecté</p>
                            <p>Joueur 2 est connecté</p>
                        </div>
                        :
                        ''
                    }
                </div>
            </section>
        )
    }
}

export default Game