import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Button } from 'semantic-ui-react'
import './style.scss'

class Home extends Component {
    render(){
        return(
            <section className="home-container">
                <header className="home-header"></header>
                <div className="home-body">
                    <h1 className="home-title">Othello</h1>
                    <Link to="/api/game">
                        <Button className="ui red basic button home-button">Je veux jouer !</Button>
                    </Link>
                </div>
                <footer className="home-footer">Made by Me with love (a bit)</footer>
            </section>
        )
    }
}

export default Home