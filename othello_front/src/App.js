import React from 'react'
import 'semantic-ui-css/semantic.min.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import './App.scss'

import Home from './scenes/Home'
import Game from './scenes/Game'

function App() {
  return (
    <main className="App">
      <Router>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/api/game' component={Game} />
          </Switch>
      </Router>
    </main>
  );
}

export default App;
