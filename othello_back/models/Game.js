const mongoose = require('mongoose')
const Schema = mongoose.Schema

const gameSchema = new Schema({
    id: String, //id du jeu - on prend l'url et on le renvoit 
    name: String, //Le nom du jeu 
    player1_id: String, //id du premier joueur
    player2_id: String, //id du deuxieme joueur
    isGameOver: Boolean,//statut du jeu Boolean, finit ou pas, si finit on le retire de la DB
    coordonates: []//Array qui contient les coordonnées des pièces
})

module.exports = mongoose.model('Game', gameSchema)
//required