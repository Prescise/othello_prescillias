const express = require('express')
const http = require('http')
const socketIo = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = socketIo(server);

io.on('connection', function(){
  io.emit('user_connected')
  console.log('user connected')

  io.on('disconnect', function(socket){
    socket.emit('user_disconnect')
    console.log('user disconnected');
  });
});





const port = 4000;
io.listen(port);
console.log('Le serveur est connecté au port ', port);
